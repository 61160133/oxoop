import java.util.InputMismatchException;
import java.util.Scanner;

class Game {

    Scanner kb = new Scanner(System.in);
    private int row;
    private int col;
    Table table = null;
    Player o = null;
    Player x = null;

    public Game() {
    	this.o = new Player('o');
    	this.x = new Player('x');
    }
    public void run() {
    	while(true) {
    		this.runOnce();
    		if(!askContinue()) {
    			return;
    		}
    	}
    }
    private int getRandomNumber(int min , int max) {
    	return (int) ((Math.random() * (max - min)) + min);
    }
    public void newGame() {
    	if(getRandomNumber(1,100) % 2 == 0) {
    		this.table = new Table(o,x);
    	}else {
    		this.table = new Table(x,o);
    	}
    }
	public void runOnce() {
    	this.showWelcome();
    	this.newGame();
    	while(true) {
    		this.showTable();
    		this.showTurn();
    		this.inputRowCol();	
    		if(table.checkWin()) {
    			if(table.getWinner() != null) {
    				showWin();
    			}else {
    				showDraw();	
    			}
    			this.showStat();
    			return;	
    		}
    		table.switchPlayer();
    	}
    }
	public void showWelcome() {
        System.out.println("+-------Welcome to OX Game------+");
  
    }
    private void showTable() {
    	char[][] board = this.table.getBoard();
    	for (int row = 0 ; row < board.length ; row++) {
    		System.out.print(" | ");
    		for (int col = 0 ; col < board[row].length ; col ++) {
    			System.out.print(board[row][col] + " | ");
    		}
    		System.out.println("");
    	}
    }
    private void showTurn() {
    	System.out.println("Turn : "+table.getCurrentPlayer().getName());
    }
    private void input() {
    	while(true) {
    		try {
    			showInput();
    			this.row = kb.nextInt();
    			this.col = kb.nextInt();
    			return;
    		}catch(InputMismatchException iE){
    			kb.next();
    			showInputNumber();
    		}
    	}	
    }
    private void inputRowCol() {
    	while(true){
    		this.input();
    		try {
    			if(table.setRowCol(row,col)) {
    				return;
    			}	
    		}catch(ArrayIndexOutOfBoundsException e){
    			showInputNumber();
    		}	
    	}
	}
    private boolean  askContinue() {
    	while(true) {
    		showContinue();
    		String ans = kb.next();
    		if(ans.equals("N")) {
    			return false;
    		}
    		else if(ans.equals("Y")) {
    			return true;
    		}
    	}
	}
    private void showStat() {
		System.out.println(o.getName() + "(WIN,LOSE,DRAW): " + o.getWin() + "," + o.getLose() + "," + o.getDraw());
		System.out.println(x.getName() + "(WIN,LOSE,DRAW): " + x.getWin() + "," + x.getLose() + "," + x.getDraw());
	}
    public void showInput() {
    	System.out.print("Please input your row and column --> ");
    }
    public void showInputNumber() {
    	System.out.println("+--- Please input number 1-3!! ---+");
    }
    private void showContinue() {
    	System.out.println();
    	System.out.println("Will you continue playing?");
    	System.out.print("Continue Y/N ? : ");
    }
    public void showWin() {
    	System.out.println("+--- " + table.getWinner().getName() + " is winner!!! ---+");
    }
    public void showDraw() {
    	System.out.println("+--- " + "Draw!!!" + "---+");
    }
}
