package ox;

import static org.junit.jupiter.api.Assertions.*;
import ox.Table;

import org.junit.jupiter.api.Test;

class TableUnitTest {

	@Test
	public void testRow3Win() {
		Player o = new Player('o'); 
		Player x = new Player('x');
		Table table = new Table(o,x);
		table.setRowCol(3, 1);
		table.setRowCol(3, 2);
		table.setRowCol(3, 3);
		assertEquals(true,table.checkWin());	
	}
	public void testCol3Win() {
		Player o = new Player('o'); 
		Player x = new Player('x');
		Table table = new Table(o,x);
		table.setRowCol(1, 3);
		table.setRowCol(2, 3);
		table.setRowCol(3, 3);
		assertEquals(true,table.checkWin());
	}
	public void testRow1Win() {
		Player o = new Player('o'); 
		Player x = new Player('x');
		Table table = new Table(o,x);
		table.setRowCol(1, 1);
		table.setRowCol(1, 2);
		table.setRowCol(1, 3);
		assertEquals(true,table.checkWin());	
	}
	public void testCol1Win() {
		Player o = new Player('o'); 
		Player x = new Player('x');
		Table table = new Table(o,x);
		table.setRowCol(1, 1);
		table.setRowCol(2, 1);
		table.setRowCol(3, 1);
		assertEquals(true,table.checkWin());
	}
	public void testRow2Win() {
		Player o = new Player('o'); 
		Player x = new Player('x');
		Table table = new Table(o,x);
		table.setRowCol(2, 1);
		table.setRowCol(2, 2);
		table.setRowCol(2, 3);
		assertEquals(true,table.checkWin());	
	}
	public void testCol2Win() {
		Player o = new Player('o'); 
		Player x = new Player('x');
		Table table = new Table(o,x);
		table.setRowCol(1, 2);
		table.setRowCol(2, 2);
		table.setRowCol(3, 2);
		assertEquals(true,table.checkWin());
	}
	public void testSwitchPlayer() {
		Player o = new Player('o'); 
		Player x = new Player('x');
		Table table = new Table(o,x);
		table.switchPlayer();
		assertEquals(x,table.getCurrentPlayer().getName());
	}
	public void testX1Win() {
		Player o = new Player('o'); 
		Player x = new Player('x');
		Table table = new Table(o,x);
		table.setRowCol(1, 1);
		table.setRowCol(2, 2);
		table.setRowCol(3, 3);
		assertEquals(true,table.checkWin());
	}
	public void testX2Win() {
		Player o = new Player('o'); 
		Player x = new Player('x');
		Table table = new Table(o,x);
		table.setRowCol(1, 3);
		table.setRowCol(2, 2);
		table.setRowCol(3, 1);
		assertEquals(true,table.checkWin());
	}
	public void TestCheckCol() {
		Player o = new Player('o'); 
		Player x = new Player('x');
		Table table = new Table(o,x);
		assertEquals(true,table.checkCol());
	}
	public void TestCheckRow() {
		Player o = new Player('o'); 
		Player x = new Player('x');
		Table table = new Table(o,x);
		assertEquals(true,table.checkRow());
	}
	
}
