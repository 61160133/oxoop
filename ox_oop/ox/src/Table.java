public class Table {
    char board [][] = {{'_','_','_'},
            		  {'_','_','_'},
                      {'_','_','_'}};
    Player first;
    Player second;
    private Player currentPlayer;
    private Player winner = null;
    int lastCol;
    int lastRow;
    int countTurn = 0;
    
    public Table( Player first,Player second){
        this.first = first;
        this.second = second;
        this.currentPlayer = this.first;
    }
    public char[][] getBoard(){
    	return board;
    }
    public Player getCurrentPlayer() {
    	return  currentPlayer;
    }
    public void switchPlayer() {
    	if(this.currentPlayer == first) {
    		this.currentPlayer = second;
    	}else {
    		this.currentPlayer = first;
    	}
    }
    public boolean setRowCol(int row , int col) {
    	if(this.board[row-1][col-1] != '_') {
    		return false;
    	}
    	this.board[row-1][col-1] = currentPlayer.getName();
    	lastCol = col - 1;
    	lastRow = row - 1;
    	countTurn ++;
    	return true;
    }
    public boolean checkRow() {
    	for(int col = 0 ; col < this.board[lastRow].length; col ++) {
    		if(this.board[lastRow][col] != currentPlayer.getName()) {
    			return false;
    		}
    	}
    	return true;
    }
    public boolean checkCol() {
    	for(int row = 0 ; row < this.board[lastRow].length; row ++) {
    		if(this.board[row][lastCol] != currentPlayer.getName()) {
    			return false;
    		}
    	}
    	return true;
    }
    public boolean checkX1() {
    	for(int i = 0 ; i < this.board.length ; i++) {
    		if(this.board[i][i] != currentPlayer.getName()) {
    			return false;
    		}
    	}
    	return true;
    }
    boolean checkX2() {
    	for(int i = 0 ; i < this.board.length ; i++) {
    		if(this.board[i][this.board.length-i-1] != currentPlayer.getName()) {
    			return false;
    		}
    	}
    	return true;
    }
    public void updateStat() {
    	if(this.first == this.winner) {
    		this.first.win();
    		this.second.lose();	
    	}
    	else if(this.second == this.winner) {
    		this.second.win();
    		this.first.lose();
    	}else {
    		this.second.draw();
    		this.first.draw();
    	}
    }
	public boolean checkWin() {
		if(checkCol() || checkRow() || checkX1() || checkX2()) {
			this.winner = currentPlayer;
			updateStat();
			return true;
		}
		if(countTurn == 9) {
			updateStat();
			return true;
		}
		return false;
	}
	public Player getWinner() {
		return winner;
	}
}